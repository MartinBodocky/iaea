﻿using IAEA.VisitorSystem.DataContracts.Entities;
using IAEA.VisitorSystem.Operations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.Persistance
{
    public class VisitorContext : DbContext, IVisitorContext
    {
        public VisitorContext()
            : base("VisitorContext")
        {
            Database.SetInitializer<VisitorContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }
        
        public virtual IDbSet<Visitor> Visitors { get; set; }
        public virtual IDbSet<Card> Cards { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        // define support overrides for easier test doubling of entity framework
        public override DbSet<TEntity> Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        public override DbSet Set(Type entityType)
        {
            return base.Set(entityType);
        }

        public void SetModified(object entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        public bool CheckDetached(object entity)
        {
            return Entry(entity).State == EntityState.Detached;
        }
    }
}
