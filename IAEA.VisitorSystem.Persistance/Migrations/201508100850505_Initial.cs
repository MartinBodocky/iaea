namespace IAEA.VisitorSystem.Persistance.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Card",
                c => new
                    {
                        CardId = c.Long(nullable: false, identity: true),
                        CheckedIn = c.Boolean(nullable: false),
                        CheckedOut = c.Boolean(nullable: false),
                        CheckInTime = c.DateTime(nullable: false),
                        CheckOutTime = c.DateTime(nullable: false),
                        Active = c.Boolean(nullable: false),
                        VisitorId = c.Long(nullable: false),
                        Visitor_VisitorId = c.Long(),
                    })
                .PrimaryKey(t => t.CardId)
                .ForeignKey("dbo.Visitor", t => t.Visitor_VisitorId)
                .ForeignKey("dbo.Visitor", t => t.VisitorId, cascadeDelete: true)
                .Index(t => t.VisitorId)
                .Index(t => t.Visitor_VisitorId);
            
            CreateTable(
                "dbo.Visitor",
                c => new
                    {
                        VisitorId = c.Long(nullable: false, identity: true),
                        VisitorName = c.String(),
                        CardId = c.Long(),
                        CreatedByStaffName = c.String(),
                    })
                .PrimaryKey(t => t.VisitorId)
                .ForeignKey("dbo.Card", t => t.CardId)
                .Index(t => t.CardId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Card", "VisitorId", "dbo.Visitor");
            DropForeignKey("dbo.Card", "Visitor_VisitorId", "dbo.Visitor");
            DropForeignKey("dbo.Visitor", "CardId", "dbo.Card");
            DropIndex("dbo.Visitor", new[] { "CardId" });
            DropIndex("dbo.Card", new[] { "Visitor_VisitorId" });
            DropIndex("dbo.Card", new[] { "VisitorId" });
            DropTable("dbo.Visitor");
            DropTable("dbo.Card");
        }
    }
}
