namespace IAEA.VisitorSystem.Persistance.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeCardDateTimesNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Card", "CheckInTime", c => c.DateTime());
            AlterColumn("dbo.Card", "CheckOutTime", c => c.DateTime());
            AlterColumn("dbo.Visitor", "VisitorName", c => c.String(nullable: false));
            AlterColumn("dbo.Visitor", "CreatedByStaffName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Visitor", "CreatedByStaffName", c => c.String());
            AlterColumn("dbo.Visitor", "VisitorName", c => c.String());
            AlterColumn("dbo.Card", "CheckOutTime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Card", "CheckInTime", c => c.DateTime(nullable: false));
        }
    }
}
