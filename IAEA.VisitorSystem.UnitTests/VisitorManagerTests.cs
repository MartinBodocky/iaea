﻿using IAEA.VisitorSystem.DataContracts.Entities;
using IAEA.VisitorSystem.Managers;
using IAEA.VisitorSystem.Operations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.UnitTests
{
    [TestClass]
    public class VisitorManagerTests
    {
        private VisitorManager manager;
        private List<Visitor> visitors;

        [TestInitialize]
        public void SetUp()
        {
            #region Test Data
            visitors = new List<Visitor>
            {
                new Visitor
                {
                    VisitorId = 1,
                    VisitorName = "Martin Bodocky",
                    CreatedByStaffName = "Joe"
                }

            };
            #endregion

            // load a few customers in mock DB
            var context = new Mock<IVisitorContext>();
            context.Setup(m => m.Visitors).Returns(visitors.SetDbSet<Visitor, long>((v, id) => v.VisitorId == id, context));

            manager = new VisitorManager(context.Object);
        }

        [TestMethod]
        public async Task GetVisitors()
        {
            // Arrange

            // Act 
            var visitors = await manager.GetVisitors();

            // Assert
            Assert.IsNotNull(visitors);
            Assert.AreEqual(visitors.Count(), 1);
        }

        [TestMethod]
        public async Task GetVisitorbyId()
        {
            // Arrange
            var visitorId = 1;

            // Act 
            var visitor = await manager.GetVisitorById(visitorId);

            // Assert
            Assert.IsNotNull(visitor);
            Assert.AreEqual(visitor.VisitorName, "Martin Bodocky");
        }

        [TestMethod]
        public async Task CreateVisitor_Success()
        {
            // Arrange
            var visitorName = "John Smith";
            var staffName = "Joe";

            // Act 
            var visitor = await manager.CreateVisitor(visitorName, staffName);

            // Assert
            Assert.IsNotNull(visitor);
            Assert.AreEqual(visitor.VisitorName, visitorName);
            Assert.AreEqual(visitor.CreatedByStaffName, staffName);
        }

        [TestMethod]
        public async Task CreateVisitor_Failure()
        {
            // Arrange
            var visitorName = "Martin Bodocky";
            var staffName = "Joe";

            // Act 
            try
            {
                var visitor = await manager.CreateVisitor(visitorName, staffName);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Visitor is already existed! Name: {0}", visitorName);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }

        [TestMethod]
        public async Task GetVisitor_Success()
        {
            // Arrange
            var visitorName = "Martin Bodocky";

            // Act 
            var visitor = await manager.GetVisitorByName(visitorName);

            // Assert
            Assert.IsNotNull(visitor);
            Assert.AreEqual(visitor.VisitorName, visitorName);
        }

        [TestMethod]
        public async Task GetVisitor_Failure()
        {
            // Arrange
            var visitorName = "John Smith";

            // Act 
            try
            {
                var visitor = await manager.GetVisitorByName(visitorName);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Visitor doesn't exist! Name: {0}", visitorName);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }
    }
}
