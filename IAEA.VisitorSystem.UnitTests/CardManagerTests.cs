﻿using IAEA.VisitorSystem.DataContracts.Entities;
using IAEA.VisitorSystem.Managers;
using IAEA.VisitorSystem.Operations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.UnitTests
{
    [TestClass]
    public class CardManagerTests
    {
        private CardManager manager;
        private List<Card> cards;
        private List<Visitor> visitors;

        [TestInitialize]
        public void SetUp()
        {
            #region Test Data
            visitors = new List<Visitor>
            {
                new Visitor
                {
                    VisitorId = 1,
                    CardId = null,
                    ActiveCard=  null,
                    HistoryCards = new List<Card>(),
                    VisitorName = "Martin Bodocky",
                    CreatedByStaffName = "Joe"
                },
                new Visitor
                {
                    VisitorId = 2,
                    CardId = null,
                    ActiveCard=  null,
                    HistoryCards = new List<Card>(),
                    VisitorName = "John Smith",
                    CreatedByStaffName = "Joe"
                },
                new Visitor
                {
                    VisitorId = 2,
                    CardId = null,
                    ActiveCard=  null,
                    HistoryCards = new List<Card>(),
                    VisitorName = "Bill Gates",
                    CreatedByStaffName = "Joe"
                },
                new Visitor
                {
                    VisitorId = 3,
                    VisitorName = "Barack Obama"
                }
            };

            cards = new List<Card>
            {
                new Card
                {
                    CardId = 1,
                    Active = true,
                    Visitor = visitors[0],
                    VisitorId = visitors[0].VisitorId
                },
                new Card
                {
                    CardId = 2,
                    Active = true,
                    CheckedIn = true,
                    CheckedOut = false,
                    CheckInTime = DateTime.Now,
                    Visitor = visitors[2],
                    VisitorId = visitors[2].VisitorId
                },
                new Card
                {
                    CardId = 3,
                    Active = true,
                    CheckedIn = false,
                    CheckedOut = true,
                    CheckInTime = DateTime.Now.AddHours(-2),
                    CheckOutTime = DateTime.Now,
                    Visitor = visitors[3],
                    VisitorId = visitors[3].VisitorId
                }
            };
            visitors[0].CardId = cards[0].CardId;
            visitors[0].ActiveCard = cards[0];
            visitors[2].CardId = cards[1].CardId;
            visitors[2].ActiveCard = cards[1];
            visitors[3].CardId = cards[2].CardId;
            visitors[3].ActiveCard = cards[2];

            #endregion

            // load a few customers in mock DB
            var context = new Mock<IVisitorContext>();
            context.Setup(m => m.Visitors).Returns(visitors.SetDbSet<Visitor, long>((v, id) => v.VisitorId == id, context));
            context.Setup(m => m.Cards).Returns(cards.SetDbSet<Card, long>((c, id) => c.CardId == id, context));

            manager = new CardManager(context.Object);
        }

        [TestMethod]
        public async Task CreateNewCard_Success()
        {
            // Arrange
            var visitorName = "John Smith";
            var visitorId = 2;

            // Act 
            var card = await manager.CreateCard(visitorId);

            // Assert
            Assert.IsNotNull(card);
            Assert.AreEqual(card.Active, true);
            Assert.IsNotNull(card.Visitor);
            Assert.AreEqual(card.Visitor.VisitorName, visitorName);
        }

        [TestMethod]
        public async Task CreateNewCard_Failure()
        {
            // Arrange
            var visitorName = "Martin Bodocky";
            var visitorId = 2;

            // Act 
            try
            {
                var visitor = await manager.CreateCard(visitorId);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Visitor has already active card! Name: {0}", visitorName);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }

        [TestMethod]
        public async Task CardCheckIn_Success()
        {
            // Arrange
            var visitorName = "John Smith";
            var visitorId = 2;
            long cardId = 1;

            // Act 
            var card = await manager.CheckInCard(cardId);

            // Assert
            Assert.IsNotNull(card);
            Assert.AreEqual(card.Active, true);
            Assert.AreEqual(card.CheckedIn, true);
            Assert.AreEqual(card.CheckedOut, false);
            Assert.AreNotEqual(card.CheckInTime, null);
            Assert.AreEqual(card.CheckOutTime, null);
        }

        [TestMethod]
        public async Task CardCheckIn_Failure()
        {
            // Arrange
            long cardId = 2;

            // Act 
            try
            {
                var visitor = await manager.CheckInCard(cardId);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Card is already checked in! CardId: {0}", cardId);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }

        [TestMethod]
        public async Task CardCheckOut_Success()
        {
            // Arrange
            long cardId = 2;

            // Act 
            var card = await manager.CheckOutCard(cardId);

            // Assert
            Assert.IsNotNull(card);
            Assert.AreEqual(card.Active, true);
            Assert.AreEqual(card.CheckedIn, false);
            Assert.AreEqual(card.CheckedOut, true);
            Assert.AreNotEqual(card.CheckInTime, null);
            Assert.AreNotEqual(card.CheckOutTime, null);
        }

        [TestMethod]
        public async Task CardCheckOut_Failure1()
        {
            // Arrange
            long cardId = 1;

            // Act 
            try
            {
                var visitor = await manager.CheckOutCard(cardId);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Card cannot be checked out, until it's not checked in!. CardId: {0}", cardId);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }
        
        [TestMethod]
        public async Task CardDeactivate_Success()
        {
            // Arrange
            long cardId = 3;

            // Act 
            var card = await manager.DeactivateCard(cardId);

            // Assert
            Assert.IsNotNull(card);
            Assert.AreEqual(card.Active, false);
            Assert.AreEqual(card.CheckedIn, false);
            Assert.AreEqual(card.CheckedOut, false);
            Assert.AreNotEqual(card.CheckInTime, null);
            Assert.AreNotEqual(card.CheckOutTime, null);
        }

        [TestMethod]
        public async Task CardDeactivate_Failure1()
        {
            // Arrange
            long cardId = 2;

            // Act 
            try
            {
                var visitor = await manager.DeactivateCard(cardId);
            }
            catch (ArgumentException exception)
            {
                var customMessage = string.Format("Card cannot be deactivated, until it's checked in!. CardId: {0}", cardId);
                // Assert
                Assert.AreEqual(exception.Message, customMessage);
            }
        }       
    }
}
