﻿using IAEA.VisitorSystem.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.Managers
{
    public class ManagerBase : IDisposable
    {
        protected IVisitorContext _context;
        public ManagerBase(IVisitorContext context)
        {
            this._context = context;
        }

        public void Dispose()
        {
            if (this._context != null)
                _context.Dispose();
        }
    }
}
