﻿using IAEA.VisitorSystem.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IAEA.VisitorSystem.DataContracts.Entities;
using System.Data.Entity;

namespace IAEA.VisitorSystem.Managers
{
    public class CardManager : ManagerBase, ICardManager
    {
        public CardManager(IVisitorContext context) : base(context)
        {

        }

        public async Task<Card> CheckInCard(long cardId)
        {
            var card = await _context.Cards.FirstAsync(c => c.CardId == cardId);
            if(card.CheckedIn)
            {
                throw new ArgumentException(string.Format("Card is already checked in! CardId: {0}", cardId));
            }
            else
            {
                card.CheckedIn = true;
                card.CheckInTime = DateTime.Now;
                card.CheckedOut = false;
                card.CheckOutTime = null;
                await _context.SaveChangesAsync();
            }

            return card;
        }

        public async Task<Card> CheckOutCard(long cardId)
        {
            var card = await _context.Cards.FirstAsync(c => c.CardId == cardId);
            if(!card.CheckedIn)
            {
                throw new ArgumentException(string.Format("Card cannot be checked out, until it's not checked in!. CardId: {0}", cardId));
            }
            else if (card.CheckedOut)
            {
                throw new ArgumentException(string.Format("Card is already checked out! CardId: {0}", cardId));
            }
            else
            {
                card.CheckedIn = false;
                card.CheckedOut = true;
                card.CheckOutTime = DateTime.Now;
                await _context.SaveChangesAsync();
            }

            return card;
        }

        public async Task<Card> CreateCard(long visitorId)
        {
            var visitor = await _context.Visitors.FirstAsync(v => v.VisitorId == visitorId);
            if(visitor.ActiveCard!= null)
            {
                throw new ArgumentException(string.Format("Visitor has already active card! Name: {0}", visitor.VisitorName));
            }
            else
            {
                var card = new Card
                {
                    Active = true,
                    CheckedIn = false,
                    CheckedOut = false,
                    Visitor = visitor,
                    VisitorId = visitor.VisitorId
                };
                _context.Cards.Add(card);

                // associate with visitor
                visitor.ActiveCard = card;

                await _context.SaveChangesAsync();
                return card;
            }
        }

        public async Task<Card> DeactivateCard(long cardId)
        {
            var card = await _context.Cards.Include(c => c.Visitor).FirstAsync(c => c.CardId == cardId);
            if (!card.CheckedOut)
            {
                throw new ArgumentException(string.Format("Card cannot be deactivated, until it's checked in!. CardId: {0}", cardId));
            }
            else
            {
                card.Active = false;
                card.CheckedIn = false;
                card.CheckedOut = false;
                card.CheckOutTime = DateTime.Now;

                // move card to visitos's history cards
                var visitor = card.Visitor;
                visitor.ActiveCard = null;
                visitor.CardId = null;
                visitor.HistoryCards.Add(card);

                await _context.SaveChangesAsync();
            }

            return card;
        }
    }
}
