﻿using IAEA.VisitorSystem.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IAEA.VisitorSystem.DataContracts.Entities;
using System.Data.Entity;

namespace IAEA.VisitorSystem.Managers
{
    public class VisitorManager : ManagerBase, IVisitorManager
    {
        public VisitorManager(IVisitorContext context) : base(context)
        {

        }

        public async Task<Visitor> CreateVisitor(string visitorName, string staffName)
        {
            var visitor = await _context.Visitors.FirstOrDefaultAsync(v => v.VisitorName == visitorName);
            if(visitor!= null)
            {
                // visitor already existed 
                throw new ArgumentException(String.Format("Visitor is already existed! Name: {0}", visitorName));
            }
            else
            {
                visitor = new Visitor
                {
                    VisitorName = visitorName,
                    CreatedByStaffName = staffName
                };
                _context.Visitors.Add(visitor);
                await _context.SaveChangesAsync();
            }
            return visitor;
        }

        public async Task<IEnumerable<Visitor>> GetVisitors()
        {
            return await _context.Visitors
                            .Include(v => v.ActiveCard)
                            .Include(v => v.HistoryCards)
                            .ToListAsync();
        }

        public async Task<Visitor> GetVisitorById(long visitorId)
        {
            return await _context.Visitors
                            .Include(v => v.ActiveCard)
                            .Include(v => v.HistoryCards)
                            .FirstOrDefaultAsync(v => v.VisitorId == visitorId);
        }

        public async Task<Visitor> GetVisitorByName(string visitorName)
        {
            var visitor = await _context.Visitors.FirstOrDefaultAsync(v => v.VisitorName == visitorName);
            if (visitor == null)
            {
                // visitor already existed 
                throw new ArgumentException(String.Format("Visitor doesn't exist! Name: {0}", visitorName));
            }
            return visitor;
        }
    }
}
