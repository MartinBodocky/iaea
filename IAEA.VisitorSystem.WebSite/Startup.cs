﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IAEA.VisitorSystem.WebSite.Startup))]
namespace IAEA.VisitorSystem.WebSite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
