﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IAEA.VisitorSystem.DataContracts.Entities;
using IAEA.VisitorSystem.Persistance;
using IAEA.VisitorSystem.Operations;
using IAEA.VisitorSystem.WebSite.Models;

namespace IAEA.VisitorSystem.WebSite.Controllers
{
    [Authorize]
    public class VisitorsController : Controller
    {
        private IVisitorManager _visitorManager;
        private ICardManager _cardManager;
        public VisitorsController(IVisitorManager visitorManager, ICardManager cardManager)
        {
            this._visitorManager = visitorManager;
            this._cardManager = cardManager;
        }

        // GET: Visitors
        public async Task<ActionResult> Index()
        {
            var visitors = await _visitorManager.GetVisitors();
            return View(visitors);
        }

        // GET: Visitors/Details/5
        public async Task<ActionResult> Details(long? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Visitor visitor = null;
            try
            {
                visitor = await _visitorManager.GetVisitorById(id.Value);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Visitors", ex.Message);
            }

            if (visitor == null)
            {
                return HttpNotFound();
            }
            return View(visitor);
        }

        // GET: Visitors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Visitors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "VisitorName,CreatedByStaffName")] VisitorModel visitorModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // create visitor
                    var visitor = await _visitorManager.CreateVisitor(visitorModel.VisitorName, visitorModel.CreatedByStaffName);

                    // create card to this visitor
                    var card = await _cardManager.CreateCard(visitor.VisitorId);
                }
                catch (Exception ex)
                {
                    return RedirectToAction("Error", "Visitors", ex.Message);
                }

                return RedirectToAction("Index");
            }

            return View(visitorModel);
        }

        public ActionResult Error(string message)
        {
            return View(message);
        }

        #region Card actions

        public async Task<ActionResult> CardCheckIn(long? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = null;
            try
            {
                card = await _cardManager.CheckInCard(id.Value);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Visitors", ex.Message);
            }
            if (card == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Details", new { id = card.VisitorId });
        }

        public async Task<ActionResult> CardCheckOut(long? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = null;
            try
            {
                card = await _cardManager.CheckOutCard(id.Value);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Visitors", ex.Message);
            }
            if (card == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Details", new { id = card.VisitorId });
        }

        public async Task<ActionResult> CardDeactivate(long? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = null;
            try
            {
                card = await _cardManager.DeactivateCard(id.Value);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Visitors", ex.Message);
            }
            if (card == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Details", new { id = card.VisitorId });
        }

        public async Task<ActionResult> CreateCard(long? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Card card = null;
            try
            {
                card = await _cardManager.CreateCard(id.Value);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Visitors", ex.Message);
            }
            if (card == null)
            {
                return HttpNotFound();
            }

            return RedirectToAction("Details", new { id = card.VisitorId });
        }
        #endregion
    }
}
