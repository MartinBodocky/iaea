﻿using IAEA.VisitorSystem.DataContracts.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IAEA.VisitorSystem.WebSite.Models
{
    public class VisitorModel
    {
        public long VisitorId { get; set; }
        [Required]
        [DisplayName("Visitor Name")]
        public string VisitorName { get; set; }
        
        public long? CardId { get; set; }

        [Required]
        [DisplayName("Staff Name")]
        public string CreatedByStaffName { get; set; }

        public Card ActiveCard { get; set; }

        public virtual ICollection<Card> HistoryCards { get; set; }
    }
}