﻿using IAEA.VisitorSystem.DataContracts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.Operations
{
    public interface IVisitorManager
    {
        Task<IEnumerable<Visitor>> GetVisitors();
        Task<Visitor> CreateVisitor(string visitorName, string staffName);
        Task<Visitor> GetVisitorById(long visitorId);
        Task<Visitor> GetVisitorByName(string visitorName);
    }
}
