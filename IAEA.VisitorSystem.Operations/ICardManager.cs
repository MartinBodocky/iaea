﻿using IAEA.VisitorSystem.DataContracts.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.Operations
{
    public interface ICardManager
    {
        Task<Card> CreateCard(long visitorId);
        Task<Card> CheckInCard(long cardId);
        Task<Card> CheckOutCard(long cardId);
        Task<Card> DeactivateCard(long cardId);
    }
}
