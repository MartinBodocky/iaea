﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IAEA.VisitorSystem.DataContracts.Entities
{
    public class Card
    {
        [Key]
        public long CardId { get; set; }
        public bool CheckedIn { get; set; }
        public bool CheckedOut { get; set; }

        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }

        public bool Active { get; set; }

        [ForeignKey("Visitor")]
        public long VisitorId { get; set; }

        public virtual Visitor Visitor { get; set; }
    }
}