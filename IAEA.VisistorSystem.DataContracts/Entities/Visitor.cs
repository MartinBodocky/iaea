﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAEA.VisitorSystem.DataContracts.Entities
{
    public class Visitor
    {
        public Visitor()
        {
            HistoryCards = new HashSet<Card>();
        }
        [Key]
        public long VisitorId { get; set; }
        [Required]
        public string VisitorName { get; set; }

        [ForeignKey("ActiveCard")]
        public long? CardId { get; set; }

        [Required]
        public string CreatedByStaffName { get; set; }

        public Card ActiveCard { get; set; }

        public virtual ICollection<Card> HistoryCards { get; set; }
    }
}
